package freecell.model;

import java.util.LinkedList;
import java.util.List;

public abstract class PileImpl implements Pile {
  protected List<Card> list;

  public PileImpl() {
    list = new LinkedList<Card>();
  }

  public abstract void addCard(Card c) throws IllegalStateException;

  public void removeCard(int index) {
    list.remove(index);
  }

  /**
   * Remove the card with given index in this pile.
   *
   * @param index the index of the card to be removed
   * @return the card removed
   * @throws IllegalStateException if this pile is empty or the card to be removed is not on the
   *                               top.
   */
  public Card findCard(int index) throws  IllegalStateException{
    if(list.size()==0){
      throw new IllegalStateException("The pile is empty.");
    }
    if(index!=(list.size()-1)){
      throw new IllegalStateException("This is not the top of the pile.");
    }
    return list.get(index);
  }

  public int getSize(){
    return 0;
  }

  @Override
  public String toString() {
    if(list.size()==0){
      return "";
    }
    StringBuilder s = new StringBuilder();
    for (int i = 0; i < list.size(); i++) {
      s.append(list.get(i).toString());
      if (i < list.size() - 1) {
        s.append(", ");
      }
    }
    return s.toString();
  }

  protected boolean valueComparedToTopCard(Card c, int different) {
    int index = list.size()-1;
    return c.getValue().compareTo(list.get(index).getValue())==different;
  }
}
