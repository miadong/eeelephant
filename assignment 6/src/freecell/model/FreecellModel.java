package freecell.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class FreecellModel implements FreecellOperations<Card> {
  private Pile [] open;
  private Pile [] cascade;
  private Pile [] foundation;

  private FreecellModel(int cascades, int opens) {
    //this.cascades = cascades;
    //this.opens = opens;
    open = new OpenPile[opens];
    for(int i =0; i< open.length;i++){
      open[i] = new OpenPile();
    }

    cascade = new CascadePile[cascades];
    for(int i =0; i< cascade.length;i++){
      cascade[i] = new CascadePile();
    }

    foundation = new FoundationPile[4];
    for(int i =0; i< foundation.length;i++){
      foundation[i] = new FoundationPile();
    }
  }

  @Override
  //club,diamond,heart,spade
  public List<Card> getDeck() {//2
    List<Card> deck = new LinkedList<>();
    Suit [] s = {Suit.Club,Suit.Diamond,Suit.Heart,Suit.Spade};
    Value [] v = {Value.A,Value.Two, Value.Three,Value.Four, Value.Five, Value.Six,
            Value.Seven, Value.Eight, Value.Nine, Value.Ten, Value.J, Value.Q,Value. K};
    for(Suit suit:s) {
      for(Value value:v) {
        deck.add(new Card(suit, value));
      }
    }
    return deck;
  }

  @Override
  public void startGame(List<Card> deck, boolean shuffle) throws IllegalArgumentException {

    if(!validDeck(deck)){
      throw new IllegalArgumentException("The deck is invalid");
    }
    if (shuffle) {
      Collections.shuffle(deck);
    }
    distribute(deck);
  }

  private boolean validDeck(List<Card> deck){//1
    //hashSet
    HashSet<Card> deckHash = new HashSet<>(deck);
    return deckHash.size()==52;
  }


  private void distribute(List<Card> deck) {//2
    while (deck.size() > 0) {
      for (int i = 0; i < cascade.length; i++) {
        if (deck.size() > 0) {
          CascadePile c = (CascadePile) cascade[i];
          cascade[i] = c.distribute(deck.get(0));
          deck.remove(0);
        } else {
          break;
        }
      }
    }
  }

  @Override
  public void move(PileType source, int pileNumber, int cardIndex,
                   PileType destination, int destPileNumber)
          throws IllegalArgumentException, IllegalStateException {
    Pile from = getPile(source, pileNumber);
    Pile to = getPile(destination,destPileNumber);
    if(from!=to) {
      to.addCard(from.findCard(cardIndex));
      from.removeCard(cardIndex);
    }
//1

  }

  private Pile getPile(PileType type, int number) throws IllegalStateException {//2
    switch (type) {
      case OPEN:
        if (number >= open.length) {
          throw new IllegalStateException("Open pile " + number + "does not exist.");
        }
        return open[number];
      case CASCADE:
        if (number >= cascade.length) {
          throw new IllegalStateException("Cascade pile " + number + "does not exist.");
        }
        return cascade[number];
      case FOUNDATION:
        if (number >= foundation.length) {
          throw new IllegalStateException("Foundation pile " + number + "does not exist.");
        }
        return foundation[number];
      default:
        return null;
    }
  }

  @Override
  public boolean isGameOver() {//1
    //foundation.size = 13
    boolean result = true;
    for (Pile p :
            foundation) {
      if(p.getSize() != 13) {
        result = false;
        break;
      }
    }
    return result;
  }

  @Override
  public String getGameState() {//2
    StringBuilder s = new StringBuilder();
    for (int i = 0; i < foundation.length; i++) {
      s.append("F");
      s.append(i+1);
      s.append(": ");
      s.append(foundation[i].toString());
      s.append("\n");
    }
    for (int i = 0; i < open.length; i++) {
      s.append("O");
      s.append(i+1);
      s.append(": ");
      s.append(open[i].toString());
      s.append("\n");
    }
    for (int i = 0; i < cascade.length; i++) {
      s.append("C");
      s.append(i+1);
      s.append(": ");
      s.append(cascade[i].toString());
      if(i< cascade.length-1) {
        s.append("\n");
      }
    }

    return s.toString();
  }

  public static FreecellBuilder getBuilder() {
    return new FreecellBuilder();
  }

  public static class FreecellBuilder implements FreecellOperationsBuilder {
    private int cascades;
    private int opens;

    public FreecellBuilder() {
      //assign default values to all the fields as above
      cascades = 8;
      opens = 4;
    }

    @Override
    public FreecellOperationsBuilder cascades (int c) throws IllegalArgumentException{
      if(c<4){
        throw new IllegalArgumentException("xxxx");
      }
      this.cascades = c;
      return this;
    }

    @Override
    public FreecellOperationsBuilder opens(int o) throws IllegalArgumentException {
      if(o<1){
        throw new IllegalArgumentException("xxxx");
      }
      this.opens = o;
      return this;
    }

    @Override
    public FreecellOperations build() {

      return new FreecellModel(cascades, opens);
    }
  }

}
