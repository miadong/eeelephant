package freecell.model;

public enum Value {
  A, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, J, Q, K;

  @Override
  public String toString() {
    switch (this) {
      case A:
        return "A";
      case Two:
        return "2";
      case Three:
        return "3";
      case Four:
        return "4";
      case Five:
        return "5";
      case Six:
        return "6";
      case Seven:
        return "7";
      case Eight:
        return "8";
      case Nine:
        return "9";
      case Ten:
        return "10";
      case J:
        return "J";
      case Q:
        return "Q";
      case K:
        return "K";
      default:
        return null;
    }
  }

}
