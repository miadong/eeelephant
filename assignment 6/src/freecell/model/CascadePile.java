package freecell.model;

public class CascadePile extends PileImpl {
  public Pile distribute(Card c) {
    list.add(c);
    return this;
  }

  @Override
  public void addCard(Card c) throws IllegalStateException {
    if(list.isEmpty()) {
      list.add(c);
    }
    else {
      //check suit and value
      if((list.get(list.size() - 1).differentColor(c)) && valueComparedToTopCard(c, -1)) {
        list.add(c);
      }
      else {
        throw new IllegalStateException("The card to bee added should "
                + "have different color with top card "
                + "and its value must one less than top card");
      }
    }
  }


}
