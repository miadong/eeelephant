package freecell.model;

public enum Suit {
  Club,Diamond,Heart,Spade;

  @Override
  public String toString(){
    switch (this){
      case Club:
        return"♣";
      case Diamond:
        return"♦";
      case Heart:
        return"♥";
      case Spade:
        return"♠";
      default:
        return null;
    }
  }

}
