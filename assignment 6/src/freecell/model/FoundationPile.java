package freecell.model;

import static freecell.model.Value.A;

public class FoundationPile extends PileImpl {
  Suit suit;

  public FoundationPile() {
    super();
    this.suit = null;
  }

  @Override
  public void addCard(Card c) throws IllegalStateException {
    if(list.isEmpty()) {
      //if value is A, add, set suit
      if(c.getValue()==A) {
        list.add (c);
        this.suit = c.getSuit();
      }
      else {
        throw new IllegalStateException("First card in foundation pile should be A");
      }
    }
    else {
      //judge suit and value
      if((c.getSuit()==this.suit) && valueComparedToTopCard(c, 1)) {
        list.add(c);
      }
      else {
        throw new IllegalStateException("Card to be added should fit in the suit "
                + "and its value must one more than top card.");
      }
    }
  }



}
