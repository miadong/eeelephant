package freecell.model;

public class OpenPile extends PileImpl {
  @Override
  public void addCard(Card c) throws IllegalStateException {
    if(list.isEmpty()){
      list.add(c);
    }
    else{
      throw new IllegalStateException("An open pile may contain at most one card.");
    }
  }
}
