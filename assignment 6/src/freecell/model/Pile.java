package freecell.model;

public interface Pile {//1

  void addCard(Card c) throws IllegalStateException;

  void removeCard(int index);

  Card findCard(int index) throws IllegalStateException;

  int getSize();

}
